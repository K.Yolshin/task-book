import {combineReducers} from 'redux'
import auth from "./auth";
import tasks from "./tasks";

const createReducer = () =>
  combineReducers({
    auth,
    tasks,
  });

export default createReducer;