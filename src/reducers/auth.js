import {LOGIN, LOGOUT, TOGGLE_MODAL} from "../constants/auth";

const auth = (state = {}, action) => {
  switch (action.type) {
    case TOGGLE_MODAL:
      return {...state, modal: !state.modal};
    case LOGIN:
      return {...state, modal: false, login: true};
    case LOGOUT:
      return {...state, login: false};
    default:
      return state;
  }
};

export default auth;