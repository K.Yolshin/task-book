import {
    CHANGE_PAGE,
    CHANGE_SORT,
    EDIT_TASK,
    FETCHING,
    GET_TASKS,
    SAVE_TASK_ERROR,
    TOGGLE_MODAL
} from "../constants/tasks";
import React from 'react';

const tasks = (state = {}, action) => {
  switch (action.type) {
    case TOGGLE_MODAL:
      return {
        ...state,
        errorMessage: '',
        editTask: null,
        modal: !state.modal
      };
    case FETCHING:
      return {
        ...state,
        fetching: true
      };
    case CHANGE_PAGE:
      return {
        ...state,
        page: action.payload
      };
    case CHANGE_SORT:
      return {
        ...state,
        sortField: action.payload.field,
        sortDirection: action.payload.direction
      };
    case GET_TASKS:
      return {
        ...state,
        modal: false,
        fetching: false,
        list: action.payload.tasks,
        totalCount: action.payload.total_task_count
      };
    case EDIT_TASK:
      return {
        ...state,
        modal: true,
        editTask: action.payload
      };
    case SAVE_TASK_ERROR: {
      const errorMessage =
        typeof action.payload === 'object' ?
            Object.keys(action.payload)
              .map(field => `${field}: ${action.payload[field]}`)
              .map((msg, index) => <p key={index}>{msg}</p>) :
          action.payload;
      return {
        ...state,
        fetching: false,
        errorMessage
      };
    }
    default:
      return state;
  }
};

export default tasks;