const moduleName = 'TASKS';
export const TOGGLE_MODAL = `${moduleName}/TOGGLE_MODAL`;
export const GET_TASKS = `${moduleName}/GET_TASKS`;
export const FETCHING = `${moduleName}/FETCHING`;
export const CHANGE_PAGE = `${moduleName}/CHANGE_PAGE`;
export const CHANGE_SORT = `${moduleName}/CHANGE_SORT`;
export const SAVE_TASK_ERROR = `${moduleName}/SAVE_TASK_ERROR`;
export const EDIT_TASK = `${moduleName}/EDIT_TASK`;