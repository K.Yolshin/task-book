const moduleName = 'AUTH';
export const TOGGLE_MODAL = `${moduleName}/TOGGLE_MODAL`;
export const LOGIN = `${moduleName}/LOGIN`;
export const LOGOUT = `${moduleName}/LOGOUT`;