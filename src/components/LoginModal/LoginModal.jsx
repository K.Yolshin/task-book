import React from 'react'
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input, Alert} from 'reactstrap';
import PropTypes from 'prop-types';
import {validate} from "../../helpers/authHelper";

class LoginModal extends React.PureComponent {

  static propTypes = {
    toggleModal: PropTypes.func,
    modal: PropTypes.bool,
  };

  state = {
    error: false,
    username: '',
    password: ''
  };

  showInvalidMsg = errors => this.setState({error: true});

  toggle = () => this.props.toggleModal();

  save = () => {
    const {username, password} = this.state,
      valid = validate(username, password);
    if (valid) {
      this.props.login();
      this.setState({username: '', password: ''});
    } else {
      this.showInvalidMsg();
    }
  };

  change = (e, name) => this.setState({
    [name]: e.target.value,
    error: false
  });

  render() {
    const {error, username, password} = this.state;
    return (
      <Modal isOpen={this.props.modal}
             toggle={this.toggle}
             backdrop={true}>
        <ModalHeader toggle={this.toggle}>Авторизация</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label for='username'>Логин:</Label>
            <Input type='text'
                   value={username}
                   onChange={e => this.change(e, 'username')}
                   name='username'
                   id='username'
                   placeholder='Введите имя пользователя'/>
          </FormGroup>
          <FormGroup>
            <Label for='password'>Пароль:</Label>
            <Input type='password'
                   value={password}
                   onChange={e => this.change(e, 'password')}
                   name='password'
                   id='password'
                   placeholder='Введите пароль'/>
          </FormGroup>
          <Alert isOpen={error} color="danger">
            Неправильный login или пароль
          </Alert>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.toggle}>Отмена</Button>
          <Button color="success" onClick={this.save}>Войти</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default LoginModal;