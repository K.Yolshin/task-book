import React from 'react';
import {Alert, Button, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Spinner} from 'reactstrap';
import PropTypes from 'prop-types';
import {Status, taskDefaultValues, taskFields} from "../../helpers/taskHelper";

class TaskModal extends React.PureComponent {

  static propTypes = {
    saveTask: PropTypes.func,
    toggleModal: PropTypes.func,
    task: PropTypes.object,
    modal: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node
    ]),
    fetching: PropTypes.bool,
    login: PropTypes.bool
  };

  static defaultProps = {
    task: {...taskDefaultValues}
  };

  state = {
    task: this.props.task
  };

  componentWillReceiveProps(nextProps) {
    const changeTask = nextProps.task !== this.props.task;

    if (changeTask && nextProps.task !== null) {
      this.setState({task: nextProps.task});
    } else if (changeTask && this.props.task !== null && nextProps.task === null) {
      this.setState({task: {...taskDefaultValues}});
    }
  }

  toggle = () => {
      this.props.toggleModal();
      this.setState({task: {...taskDefaultValues}});
  };

  save = () => this.props.saveTask(this.state.task);

  changeField = (e, name) =>
    this.setState({
      task: {
        ...this.state.task,
        [name]: e.target.value
      },
      errors: []
    });

  completeTask = () => {
    const {task} = this.state,
      status = task.status ? +task.status : Status.Not_Complete;

    this.setState({
      task: {
        ...task,
        status: status === Status.Not_Complete ? Status.Complete : Status.Not_Complete
      }
    });
  };

  render() {
    const {task} = this.state,
      {fetching, modal, errorMessage, login} = this.props,
      status = task.status ? +task.status : Status.Not_Complete,
      hasError = !!errorMessage,
      hasId = !!task.id;
    return (
      <Modal isOpen={modal}
             toggle={this.toggle}
             backdrop={true}>
        <ModalHeader toggle={this.toggle}>{hasId ? `Задача ${task.id}` : 'Новая задача'}</ModalHeader>
        <ModalBody>
          {taskFields.map(field =>
            <FormGroup key={field.name}>
              <Label for={field.name}>{field.title}:</Label>
              <Input type={field.type}
                     disabled={!field.changeable && hasId}
                     value={task[field.name]}
                     onChange={e => this.changeField(e, field.name)}
                     name={field.name}
                     id={field.name}
                     placeholder={field.placeholder}/>
            </FormGroup>
          )}
          {login && hasId && <FormGroup check>
            <Label check>
              <Input type="checkbox" checked={status === Status.Complete} onChange={this.completeTask}/>
              Выполнено
            </Label>
          </FormGroup>}
          <Alert isOpen={hasError} color="danger">{errorMessage}</Alert>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.toggle}>Отмена</Button>
          <Button color="success" disabled={fetching} onClick={this.save}>
            {fetching ? <Spinner size="sm" color="success"/> : 'Сохранить'}
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default TaskModal;