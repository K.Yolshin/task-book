import React, {PureComponent} from 'react';
import {Button, Col, Container, Pagination, PaginationItem, PaginationLink, Row, Table} from 'reactstrap';
import classNames from 'classnames';
import styles from './TaskList.module.css'
import TaskModalContainer from "../../containers/TaskModalContainer";
import PropTypes from 'prop-types';
import {taskHeaderFields} from "../../helpers/taskHelper";
import LoginModalContainer from "../../containers/LoginModalContainer";

class TaskList extends PureComponent {

  static propTypes = {
    toggleModal: PropTypes.func,
    toggleLoginModal: PropTypes.func,
    getTasks: PropTypes.func,
    list: PropTypes.array,
    totalCount: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    page: PropTypes.number,
    sortField: PropTypes.string,
    sortDirection: PropTypes.string,
    login: PropTypes.bool
  };

  taskPerPage = 3;

  componentDidMount() {
    this.props.getTasks();
  }

  getPageCount = () => Math.ceil(this.props.totalCount / this.taskPerPage);

  changePage = number => {
    if (this.props.page !== number) {
      this.props.changePage(number);
    }
  };

  getTasksByPage = () => this.props.list.slice(0, this.taskPerPage);
  showTaskModal = () => this.props.toggleModal();
  showLoginModal = () => this.props.toggleLoginModal();
  logout = () => this.props.logout();

  sortBy = (field, direction = 'asc') => {
    const {sortField, sortDirection} = this.props;
    if (sortField === field) {
      this.props.sortBy(field, sortDirection === 'asc' ? 'desc' : 'asc');
    } else {
      this.props.sortBy(field, direction);
    }
  };

  editTask = task => this.props.editTask(task);

  renderTable() {
    const {sortField, sortDirection, login} = this.props,
      tasks = this.getTasksByPage();
    return (
      <Table className={styles.tasks__table}>
        <thead>
        <tr>
          {taskHeaderFields.map(field =>
            <th key={field.name}
                scope="row"
                className={classNames(styles['tasks__row'], {
                  [styles['tasks__row--sort']]: field.sortable,
                  [styles['tasks__row--asc']]: sortField === field.name && sortDirection === 'asc',
                  [styles['tasks__row--desc']]: sortField === field.name && sortDirection === 'desc',
                })}
                onClick={field.sortable ? () => this.sortBy(field.name) : null}>{field.title}
            </th>
          )}
          {login ? <th>Изменение</th> : null}
        </tr>
        </thead>
        <tbody>
        {tasks.map(task =>
          <tr key={task.id}>
            <th>{task.id}</th>
            <td>{task.username}</td>
            <td>{task.email}</td>
            <td>{task.text}</td>
            <td>{task.status === 0 ? 'Не выполнена' : 'Выполнена'}</td>
            {login ? <td className={styles['tasks__row--edit']} onClick={() => this.editTask(task)}>Редактировать</td> : null}
          </tr>
        )}
        </tbody>
      </Table>
    );
  }

  render() {
    const {page, login} = this.props,
      pageCount = this.getPageCount();
    return (
      <React.Fragment>
        <Container className={styles.tasks}>

          <Row>
            <Col size='2'>
              <Button className={styles.tasks__btn} color="primary" onClick={this.showTaskModal}>
                Добавить задачу
              </Button>
            </Col>
            <Col sm={{size: 'auto', offset: 8}}>
              <Button className={styles.tasks__btn} color="primary" onClick={login ? this.logout : this.showLoginModal}>
                {login ? 'Выйти' : 'Войти'}
              </Button>
            </Col>
          </Row>

          {this.renderTable()}

          <Pagination aria-label="Пагинация">
            {[...new Array(pageCount)].map((item, index) =>
              <PaginationItem key={`page_${index + 1}`} active={page === index + 1}>
                <PaginationLink onClick={() => this.changePage(index + 1)}>
                  {index + 1}
                </PaginationLink>
              </PaginationItem>
            )}
          </Pagination>
        </Container>

        <TaskModalContainer/>
        <LoginModalContainer/>
      </React.Fragment>
    );
  }
}

export default TaskList;