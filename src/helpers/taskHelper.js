export const taskHeaderFields = [
  {
    title: '#',
    name: 'id',
    sortable: true
  },
  {
    title: 'Имя пользователя',
    name: 'username',
    sortable: true
  },
  {
    title: 'E-mail',
    name: 'email',
    sortable: true
  },
  {
    title: 'Описание задачи',
    name: 'text',
    sortable: false
  },
  {
    title: 'Статус',
    name: 'status',
    sortable: true
  }
];

export const taskDefaultValues = {
  email: '',
  username: '',
  text: ''
};

export const taskFields = [
  {
    title: 'E-mail*',
    name: 'email',
    placeholder: 'Введите email',
    type: 'email',
    changeable: false,
  },
  {
    title: 'Имя пользователя*',
    name: 'username',
    placeholder: 'Введите имя пользователя',
    type: 'text',
    changeable: false,
  },
  {
    title: 'Описание задачи*',
    name: 'text',
    placeholder: 'Введите описание задачи',
    type: 'textarea',
    changeable: true,
  }
];

export const Status = {
  Complete: 10,
  Not_Complete: 0
};
