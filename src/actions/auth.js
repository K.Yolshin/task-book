import {LOGIN, LOGOUT, TOGGLE_MODAL} from "../constants/auth";

const toggleModal = () => {
  return {
    type: TOGGLE_MODAL
  }
};

const login = () => {
  return {
    type: LOGIN
  }
};

const logout = () => {
  return {
    type: LOGOUT
  }
};

export {
  login,
  logout,
  toggleModal
}