import {
  CHANGE_PAGE, CHANGE_SORT, EDIT_TASK, FETCHING, GET_TASKS, SAVE_TASK_ERROR, TOGGLE_MODAL
} from "../constants/tasks";
import TasksService from '../modals/tasks';

const getTasks = (params = {}) =>
  (dispatch, getState) => {
    const tasksState = getState().tasks,
      page = tasksState.page,
      sort_field = tasksState.sortField,
      sort_direction = tasksState.sortDirection;
    TasksService.getTasks({page, sort_field, sort_direction})
      .then(result => {
        dispatch({
          type: GET_TASKS,
          payload: result
        })
      })
  };

const saveTask = task =>
  dispatch => {
    dispatch({
      type: FETCHING,
    });
    const promiseTask = task.id ? TasksService.updateTask : TasksService.saveTask;
    promiseTask(task)
      .then(result => {
        if (result.data.status === 'error') {
          dispatch({
            type: SAVE_TASK_ERROR,
            payload: result.data.message
          });
        } else {
          dispatch(getTasks());
        }
      })
  };

const editTask = task => ({
  type: EDIT_TASK,
  payload: task
});

const changePage = page =>
  dispatch => {
    dispatch({
      type: CHANGE_PAGE,
      payload: page
    });
    dispatch(getTasks());
  };

const sortBy = (field, direction) =>
  dispatch => {
    dispatch({
      type: CHANGE_SORT,
      payload: {field, direction}
    });
    dispatch(getTasks());
  };

const toggleModal = () => {
  return {
    type: TOGGLE_MODAL
  }
};

export {
  sortBy,
  toggleModal,
  changePage,
  getTasks,
  saveTask,
  editTask
};