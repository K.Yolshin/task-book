import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoginModal from "../components/LoginModal/LoginModal";
import * as authActions from '../actions/auth';

const mapState = state => {
  return {
    modal: state.auth.modal,
  };
};

const mapDispatch = dispatch => bindActionCreators(authActions, dispatch);

export default connect(mapState, mapDispatch)(LoginModal);