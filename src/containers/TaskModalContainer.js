import {connect} from 'react-redux';
import TaskModal from "../components/TaskModal/TaskModal";
import {toggleModal, saveTask} from "../actions/tasks";

const mapState = state => {
  return {
    modal: state.tasks.modal,
    task: state.tasks.editTask,
    fetching: state.tasks.fetching,
    errorMessage: state.tasks.errorMessage,
    login: state.auth.login
  };
};

const mapDispatch = dispatch => {
  return {
    saveTask: task => dispatch(saveTask(task)),
    toggleModal: () => dispatch(toggleModal())
  }
};

export default connect(mapState, mapDispatch)(TaskModal);