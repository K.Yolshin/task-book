import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TaskList from "../components/TaskList/TaskList";
import * as taskActions from '../actions/tasks';
import {logout, toggleModal} from "../actions/auth";

const mapState = state => {
  return {
    list: state.tasks.list,
    totalCount: state.tasks.totalCount,
    page: state.tasks.page,
    sortField: state.tasks.sortField,
    sortDirection: state.tasks.sortDirection,
    login: state.auth.login
  };
};

const mapDispatch = dispatch => ({
  ...bindActionCreators(taskActions, dispatch),
  toggleLoginModal: () => dispatch(toggleModal()),
  logout: () => dispatch(logout())
});

export default connect(mapState, mapDispatch)(TaskList);