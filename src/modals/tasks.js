import axios from 'axios';
import md5 from 'md5';

class TasksService {

  axiosInstance = null;
  developer = 'Yolshin';
  baseURL = 'https://uxcandy.com/~shapoval/test-task-backend';
  getUrl = `/?developer=${this.developer}`;
  saveUrl = `/create?developer=${this.developer}`;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: this.baseURL,
    });
  }

  encodeQueryParams = (params = {}) =>
    Object.keys(params)
      .filter(param => !!params[param])
      .map(param => `${param}=${params[param]}`)
      .join('&');

  getUpdateUrl = id => `/edit/${id}/?developer=${this.developer}`;

  getTasks = params => {
    const encodeParams = this.encodeQueryParams(params),
      url = encodeParams ? `${this.getUrl}&${encodeParams}` : `${this.getUrl}`;

    return (
      this.axiosInstance
        .get(url)
        .then(result => result.data.message)
    );
  };

  saveTask = task => {
    const form = new FormData();
    form.append("username", task.username);
    form.append("email", task.email);
    form.append("text", task.text);

    return this.axiosInstance.post(this.saveUrl, form);
  };

  updateTask = task => {
    const url = this.getUpdateUrl(task.id),
      form = new FormData(),
      signature = md5(`status=${task.status}&text=${encodeURIComponent(task.text)}&token=beejee`);
    form.append("status", task.status);
    form.append("text", task.text);
    form.append("token", "beejee");
    form.append("signature", signature);

    return this.axiosInstance.post(url, form);
  }

}

export default new TasksService();