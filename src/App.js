import React, {Component} from 'react';
import TaskListContainer from "./containers/TaskListContainer";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <TaskListContainer/>
      </React.Fragment>
    );
  }
}

export default App;
