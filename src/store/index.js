import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension';
import createReducer from '../reducers/index'

const initialState = {
  auth: {
    modal: false,
    login: false,
  },
  tasks: {
    fetching: false,
    modal: false,
    totalCount: 0,
    page: 1,
    sortField: '',
    sortDirection: '',
    errorMessage: null,
    editTask: undefined,
    list: []
  }
};

const store = createStore(createReducer(), initialState, composeWithDevTools(
  applyMiddleware(thunk)
));

export default store;
